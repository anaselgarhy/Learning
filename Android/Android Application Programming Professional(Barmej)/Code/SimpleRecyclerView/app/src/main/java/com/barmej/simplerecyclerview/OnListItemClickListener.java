package com.barmej.simplerecyclerview;

public interface OnListItemClickListener {
    public void onItemClick(int position);
}
