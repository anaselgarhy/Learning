Gift gift1 = new Gift();
        gift1.setName(R.string.damask_rose);
        gift1.setPicture(R.drawable.gift_1);
        mGifts[0] = gift1;

        Gift gift2 = new Gift();
        gift2.setName(R.string.flower);
        gift2.setPicture(R.drawable.gift_2);
        mGifts[1] = gift2;

        Gift gift3 = new Gift();
        gift3.setName(R.string.cake);
        gift3.setPicture(R.drawable.gift_3);
        mGifts[2] = gift3;

        Gift gift4 = new Gift();
        gift4.setName(R.string.laptop);
        gift4.setPicture(R.drawable.gift_4);
        mGifts[3] = gift4;

        Gift gift5 = new Gift();
        gift5.setName(R.string.mobile);
        gift5.setPicture(R.drawable.gift_5);
        mGifts[4] = gift5;

        Gift gift6 = new Gift();
        gift6.setName(R.string.book);
        gift6.setPicture(R.drawable.gift_6);
        mGifts[5] = gift6;

        Gift gift7 = new Gift();
        gift7.setName(R.string.piece_of_cake);
        gift7.setPicture(R.drawable.gift_7);
        mGifts[6] = gift7;

        Gift gift8 = new Gift();
        gift8.setName(R.string.shirt);
        gift8.setPicture(R.drawable.gift_8);
        mGifts[7] = gift8;

        Gift gift9 = new Gift();
        gift9.setName(R.string.shoe);
        gift9.setPicture(R.drawable.gift_9);
        mGifts[8] = gift9;

        Gift gift10 = new Gift();
        gift10.setName(R.string.diamond);
        gift10.setPicture(R.drawable.gift_10);
        mGifts[9] = gift10;