package com.anas.learning.kotlin.fund

fun main() {
    var i  = 4
    println("i :" + i)

    val d = 6.77657;

    i = d.toInt()

    println("i : " + i)

}