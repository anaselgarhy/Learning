# -------------------
# type()
# All Data in Python is Object
# -------------------

print(type(100)) # int => Integer
print(type(-30)) # int => Integer

print(type(3.5)) # float => Floating Point Number
print(type(-3.9998)) # float => Floating Point Number

print(type("Anas")) # str => String

print(type([1, 2, 3, 4, 5])) # list => List of Items

print(type((1, 2, 3, 4, 5))) # tuple => Tuple

print(type({"One" : 1, "Tow" : 2, "Three" : 3})) # dict => Dictionary

print(type(3 == 4)) # bool => Boolean