# ---------------------
# Escape Sequences Characters
# \b => Back Space
# \ => Escape New line
# \\ => Back Slash
# \' => Single Qute
# \" => Double Qute
# \n => New Line
# \t => Horizontall Tab
# \r => Carrriage Return
# \x{Hex Value} => Character Hex Value
# ---------------------

# Back Space
print("Hello\bAnas") # Remove o character

# Escape new line
print("Hi \
I'm \
Anas");

# Include Back Slash in string
print("I love \\")

# Include Single qute in staring
print("\'Hi\'")

# Include Double Qute in string
print("\"Anas\"")

# Include new lins in string
print("Hi,\nAnas")

# Include horizontal tab in string
print("Hi,\tAnas")

# Rplace
print("123456\rAnas")

# Include charcter from hex vale in string
print("Ana\x73")