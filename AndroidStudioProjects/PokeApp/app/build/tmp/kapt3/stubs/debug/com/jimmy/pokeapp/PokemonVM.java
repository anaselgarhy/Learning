package com.jimmy.pokeapp;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 1}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u001d\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/jimmy/pokeapp/PokemonVM;", "Landroidx/lifecycle/ViewModel;", "()V", "pokemon", "Landroidx/lifecycle/LiveData;", "", "Lcom/jimmy/pokeapp/Pokemon;", "getPokemon", "()Landroidx/lifecycle/LiveData;", "app_debug"})
public final class PokemonVM extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.LiveData<java.util.List<com.jimmy.pokeapp.Pokemon>> pokemon = null;
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.util.List<com.jimmy.pokeapp.Pokemon>> getPokemon() {
        return null;
    }
    
    public PokemonVM() {
        super();
    }
}