#include<stdio.h>

void main(void){

    int x=10;

    //Print value
    printf("Value of x is: %d\n", x);

    //Print address with decimal
    printf("Address of x with decimal is: %u\n", &x);

    //Print address with hexadecimal
    printf("Address of x with hexadecimal is: %p\n", &x);






}
