#! /usr/bin/bash

# File conditions 

# -d Test if file is a directory
# -e Test if file exists
# -f Test if file is a regular file
# -r Test if file is readable
# -w Test if file is writable
# -x Test if file is executable
# -g Test if file is a group file
# -s True if file has nonzero size
# -z True if file has zero size
# -u True if the user id is set on a file