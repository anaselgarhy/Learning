#! /usr/bin/bash
# To get the upper line, use the command: which bash, which is the command to get the path of the bash shell

# Echo command
echo "Hello bash!"

# Variables
# Uppercase by convention
# Letters, numbers, underscores

NAME="Anas"
AGE=18

# Use $VAR or ${VAR} to reference variables
echo "My name is $NAME"
echo "My age is ${AGE}"

# Use \$ to print $
echo "My money is 100\$"