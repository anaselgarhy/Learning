package com.anas.learning.springboot.ulrev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserLoginAndRegistrationBackendEmailVerificationApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserLoginAndRegistrationBackendEmailVerificationApplication.class, args);
    }

}
