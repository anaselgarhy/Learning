package com.anas.learning.springboot.ulrev.user;

/**
 * @author: <a href="https://github.com/anas-elgarhy">Anas Elgarhy</a>
 * @date: 9/11/22
 */
public enum UserRole {
    USER,
    ADMIN
}
