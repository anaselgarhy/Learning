fn main() {
    // Hello, world
    
    // In Rust, the idiomatic commit style starts a commit with tow slashes, and tthe commit cintinues until the end of the line.

    // So we're doing something complicated here, long enough that we need
    // multiple lines of comments to do it! Whew! Hopefully, this comment will
    // explain what's going on.

    let num = 7; // hhh, comment 

    // But you'll more often sleee them used in this format!
    
    // hhh, comment
    let num = 7;

    // With the comment on a separate line above the code it's annotating

    /// Rust also has another kind of comment, documentation comments
}
