fn main() {
    println!("i8:\n\t min: {}\tmax: {}", i8::MIN, i8::MAX);
    println!("u8:\n\t min: {}\tmax: {}", u8::MIN, u8::MAX);
    println!("i16:\n\t min: {}\tmax: {}", i16::MIN, i16::MAX);
    println!("u16:\n\t min: {}\tmax: {}", u16::MIN, u16::MAX);
    println!("i32:\n\t min: {}\tmax: {}", i32::MIN, i32::MAX);
    println!("u32:\n\t min: {}\tmax: {}", u32::MIN, u32::MAX);
    println!("i64:\n\t min: {}\tmax: {}", i64::MIN, i64::MAX);
    println!("u64:\n\t min: {}\tmax: {}", u64::MIN, u64::MAX);

    println!("\nf32:\n\t min: {}\tmax: {}", f32::MIN, f32::MAX);
    println!("f64:\n\t min: {}\tmax: {}", f64::MIN, f64::MAX);
}
