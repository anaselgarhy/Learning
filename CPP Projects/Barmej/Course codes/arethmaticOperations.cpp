#include <iostream>

using namespace std;

int main(void)
{
    int apple = 40, orange = 20;

    int sum = apple + orange;
    int dif = apple - orange;
    int mul = apple * orange;
    int div = apple / orange;

    cout << "Sum = " << sum << endl;
    cout << "Dif = " << dif << endl;
    cout << "Mul = " << mul << endl;
    cout << "Div = " << div << endl;
    return 0;
}
