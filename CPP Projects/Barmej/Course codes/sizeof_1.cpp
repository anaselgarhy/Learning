#include <iostream>
using namespace std;

int main(void)
{

    cout << "Size of integer = " << sizeof(int) << " byte" << endl;
    cout << "Size of float = " << sizeof(float) << " byte" << endl;
    cout << "Size of double = " << sizeof(double) << " byte" << endl;
    cout << "Size of charactar = " << sizeof(char) << " byte" << endl;
    cout << "Size of string = " << sizeof(string) << " byte" << endl;
    cout << "Size of boolen = " << sizeof(bool) << " byte" << endl;
    return 0;
}
