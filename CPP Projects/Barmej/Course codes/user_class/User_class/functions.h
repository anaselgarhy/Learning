#include "User.h"
#include <fstream>
#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

void newUser(int, User*);
void logIn(int, User*);

#endif // FUNCTIONS_H_INCLUDED
