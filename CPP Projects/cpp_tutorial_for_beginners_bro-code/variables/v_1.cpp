#include <iostream>

using namespace  std;

int main() {
	int year = 2022;
    double salary = 1000.0;
	char status = 'N';
	string model = "BMW";
	bool is_available = true;

	cout << "Year: " << year << endl;
	cout << "Salary: " << salary << endl;
	cout << "Status: " << status << endl;
	cout << "Model: " << model << endl;
	cout << "Is available: " << is_available << endl;

	return 0;
}
