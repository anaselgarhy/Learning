#include <iostream>
#include <cmath>

int main() {
	double num;
	std::cout << "Enter a number: ";
	std::cin >> num;

	// Print the square root of the number
	std::cout << "The square root of " << num << " is " << sqrt(num) << std::endl;
}
