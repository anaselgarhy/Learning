#include <iostream>
#include <cmath>

int main() {
	double num;

	std::cout << "Enter a number: ";
	std::cin >> num;

	std::cout << "The absolute value of " << num << " is " << std::abs(num) << std::endl;
}
