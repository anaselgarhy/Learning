package static1.NumObjectsOfA;

public class Main {
    public static void main(String[] args){
        A a1 = new A();
        A a2 = new A();
        A a3 = new A();
        System.out.println("Number objects of A class:  = " + A.getCount());
    }

}
