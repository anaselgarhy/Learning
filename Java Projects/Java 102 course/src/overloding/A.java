package overloding;

public class A {
    public void demo(){
        System.out.println("Demo");
    }
    public void demo(int a){
        System.out.println("Demo");
    }
    public void demo(int a, int b){
        System.out.println("Demo");
    }
    public int demo(int a ,  double b){
        System.out.println("Demo");
        return a;
    }
}
