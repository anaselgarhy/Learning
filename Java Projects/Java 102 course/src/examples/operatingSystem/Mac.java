package examples.operatingSystem;

public class Mac extends OperatingSystem {
    public Mac(){
        super("Mac", "Unknown");
    }
    public Mac(String version){
        super("Mac", version);
    }
}
