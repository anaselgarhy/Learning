import javax.swing.*;

public class MainClass {
    public static void main(String[] args){
        BarmejFrame barmejFrame = new BarmejFrame();
        barmejFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        barmejFrame.setSize(950,950);
        barmejFrame.setVisible(true);
        barmejFrame.setLocationRelativeTo(null);
    }
}
