package com.company;

public class Main {

    public static void main(String[] args) {
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder(10);
        StringBuilder sb3 = new StringBuilder("برمج");

        System.out.println("sb1: " + "(" + sb1.toString() + ")");
        System.out.println("sb2: " + "(" + sb2.toString() + ")");
        System.out.println("sb3: " + "(" + sb3.toString() + ")");
    }
}
