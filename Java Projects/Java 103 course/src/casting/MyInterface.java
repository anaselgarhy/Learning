package casting;

public interface MyInterface {
    void print();
    int getData();
}
