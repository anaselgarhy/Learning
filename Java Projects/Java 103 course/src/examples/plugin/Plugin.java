package examples.plugin;

public interface Plugin {
    void start();
    void execute();
    void stop();
}
