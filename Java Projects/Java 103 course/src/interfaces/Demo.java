package interfaces;

public class Demo implements MyInterface, Plugin{
    @Override
    public void print() {

    }
    @Override
    public int getData(int num) {
        return 0;
    }
    @Override
    public void loadPlugin() {

    }
    @Override
    public void execute() {

    }
    @Override
    public void close() {

    }
}
