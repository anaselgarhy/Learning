package abstract1;

public abstract class First {
    public int value;
    public void printValue(){
        System.out.println("value = " + value);
    }
    public abstract void print(); // Look here
}
