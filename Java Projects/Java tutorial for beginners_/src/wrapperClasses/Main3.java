package wrapperClasses;

public class Main3 {
    public static void main(String[] args) {
        Integer num = 10;
        
        // Wrapper class for primitive int
        int i = num; // unboxing (converting from wrapper to primitive)

        System.out.println("i = " + i);
    }
}
