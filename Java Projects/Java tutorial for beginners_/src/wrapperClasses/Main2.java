package wrapperClasses;

public class Main2 {
    public static void main(String[] args) {
        Byte b = 2; // Autoboxing 2 from primitive type byte to wrapper class Byte
        Short s = 3; // Autoboxing 3 from primitive type short to wrapper class Short
        Integer i = 4; // Autoboxing 4 from primitive type int to wrapper class Integer
        Long l = 5L; // Autoboxing 5 from primitive type long to wrapper class Long
        Float f = 6.0f; // Autoboxing 6.0 from primitive type float to wrapper class Float
        Double d = 7.0; // Autoboxing 7.0 from primitive type double to wrapper class Double
        Boolean bool = true; // Autoboxing true from primitive type boolean to wrapper class Boolean
        Character c = 'a'; // Autoboxing 'a' from primitive type char to wrapper class Character

        System.out.println(b);
        System.out.println(s);
        System.out.println(i);
        System.out.println(l);
        System.out.println(f);
        System.out.println(d);
        System.out.println(bool);
        System.out.println(c);
    }
}
